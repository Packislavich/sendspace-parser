#coding: utf-8
import os, sys, requests, random, string, time, threading
Result = 'Good.txt'
Checked = 0
Goods = 0

def SendSpace():

	Site = 'https://www.sendspace.com/file/'
	Page='0'
	dly = 0.1
	n = 0
	global Result
	global Checked
	global Goods
	while True: 
		RndIndx=''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(6))
		while not Page=='':
			send=requests.get(Site+RndIndx)
			Page=send.text
			print(RndIndx)
			time.sleep(dly)
			if Page=='':
				Page='0'
				dly += 0.1
				n = 0
				continue
			else:
				break
		Checked += 1
		Idn1 = Page.find('<meta property="og:title" content="')
		Idn2 = Page.find('Sorry, the file you requested is not available.')
		if Idn1 > -1 and Idn2 == -1:
			Goods += 1
			FileName = Page[Idn1+35:Page.find(' - SendSpace.com" />')]
			print(str(Goods)+'.SendSpace '+RndIndx+' ['+str(round((Goods/Checked)*100, 4))+'%] '+FileName)
			with open(Result, 'a') as p:
				p.write('SendSpace '+RndIndx+' '+FileName+'\n')
		if n > 100 and dly > 0:
			dly -= 0.1
			n = 0
		n += 1

t1 = threading.Thread(target=SendSpace)

t1.start()